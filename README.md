# Portable Alex Berowski Laptop Operations (PABLO) v2

## Installation:

On an Arch-based distribution as root, run the following:

```
curl -LO berowski.com/pablo.sh
sh pablo.sh
```

## What is PABLO?

PABLO is my personal fork of Luke's Auto-Rice Bootstrapping Scripts ([LARBS](https://github.com/LukeSmithxyz/LARBS)) by
Luke Smith ([lukesmith.xyz](https://www.lukesmith.xyz)). LARBS is a script that autoinstalls and autoconfigures
a fully-functioning and minimal terminal-and-vim-based Arch Linux environment.
Much of this README is also modified from LARBS.

PABLO can be run on a fresh install of Arch or Artix Linux.

## Customization

PABLO uses the programs [here in progs.csv](static/progs.csv) and installs
[a dotfiles repo (alexrice) here](https://gitlab.com/AlexBerowski/alexrice).

### The `progs.csv` list

PABLO will parse the given programs list and install all given programs. Note
that the programs file must be a three column `.csv`.

The first column is a "tag" that determines how the program is installed, ""
(blank) for the main repository, `A` for via the AUR or `G` if the program is a
git repository that is meant to be `make && sudo make install`ed.

The second column is the name of the program in the repository, or the link to
the git repository, and the third column is a description (should be a verb
phrase) that describes the program. During installation, PABLO will print out
this information in a grammatical sentence. It also doubles as documentation
for people who read the CSV and want to install my dotfiles manually.

You may want to tactically order the programs in your programs file. PABLO
will install from the top to the bottom.

If you include commas in your program descriptions, be sure to include double
quotes around the whole description to ensure correct parsing.

### The script itself

The script is extensively divided into functions for easier readability and
trouble-shooting. Most everything should be self-explanatory.

The main work is done by the `installationloop` function, which iterates
through the programs file and determines based on the tag of each program,
which commands to run to install it. You can easily add new methods of
installations and tags as well.

Note that programs from the AUR can only be built by a non-root user. What
PABLO does to bypass this by default is to temporarily allow the newly created
user to use `sudo` without a password (so the user won't be prompted for a
password multiple times in installation). This is done ad-hocly, but
effectively with the `newperms` function. At the end of installation,
`newperms` removes those settings, giving the user the ability to run only
several basic sudo commands without a password (`shutdown`, `reboot`,
`pacman -Syu`).
